# Conoscenze
**Full stack web development**
*  [Corso](#corso-fullstack-web-development)

**Java**
*  OOP
*  Design Patterns
    * [Strategy pattern](#strategy-pattern)
    * [Observer pattern](#observer-pattern)
*  Spring
*  Java EE
*  Hibernate
*  JPA
*  Apache Kafka
*  Junit
*  Maven
*  [Mockito](#mockito)

**IDE**
*  Eclipse
*  Intellij
*  Visual Studio Code
*  Atom
*  Emacs

**Database**
*  MySql
*  Oracle
*  Db2

**Framework**
*  [Swagger](#swagger)

**Tool**
*  Atlassian tools
    *  Bitbucket
    *  Confluence
    *  Jira
*  Postman
*  SoapUI

**VCS**
*  git
*  bitbucket

**Altri Temi**
*  [Computer Networks](#computer-networks)
*  [http](#http)
*  [https](#https)
*  [Information Systems development](#information-systems-development)
*  REST
*  [SOAP](#soap)

### corso fullstack web development
[Corso fullstack web development](https://fullstackopen.com/en)

### swagger
Tradotto dall'inglese - Swagger è un framework software open-source supportato da un ampio ecosistema di strumenti che aiuta gli sviluppatori a progettare, 
costruire, documentare e utilizzare i servizi Web RESTful. Wikipedia (inglese)

### mockito
Tradotto dall'inglese - Mockito è un framework di testing open source per Java rilasciato sotto licenza MIT. 
Il framework consente la creazione di test di oggetti doppi in test unitari automatizzati allo scopo di sviluppo basato su test o sviluppo comportamentale. 
Il nome e il logo della struttura sono un gioco di mojito, un tipo di bevanda. Wikipedia (inglese)

### Strategy pattern

Questa spiegazione è presa da [Youtube Derek Banas "Strategy Pattern" dalla playlist Designs Patterns](https://www.youtube.com/watch?v=-NCgRD9-C6o)  
Un ottimo libro per i design patterns in Java è [Head First Design Patterns](https://www.amazon.it/Head-First-Design-Patterns-Freeman/dp/0596007124/ref=asc_df_0596007124/?tag=googshopit-21&linkCode=df0&hvadid=90702855060&hvpos=1o1&hvnetw=g&hvrand=2629379847264260306&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=1008380&hvtargid=pla-138214723035&psc=1)

**Definizione**  

Lo Strategy Pattern definisce una famiglia di algoritmi, incapsula ognuno di essi, e li fa intercambiabili, lasciando variare l'algoritmo indipendentemente dai clienti che lo usano.

**Quando usare lo Strategy Pattern**

*  Quando si vuole definire una classe che avrà un comportamento simile ad altri comportamenti in una lista
    * l'oggetto della classe deve poter scegliere tra:
        * non volo
        * volo con le ali
        * volo super velocemente
    * quando si ha bisogno di usare dinamicamente uno di questi comportamenti

**Descrizione**

Creo un'interfaccia pubblica "Flys.java" e all'interno di questa interfaccia:  

*  definisco un metodo fly che restituisce una stringa  

        String fly();
        
*  definisco tante classi, che implementano l'interfaccia, per quanti sono i modi in cui un animale può volare o non può volare, ad esempio:

        //Class used if the Animal can fly

        class ItFlys implements Flys{

	        public String fly() {
		
		        return "Flying High";
		
	        }
	
        }

        //Class used if the Animal can't fly

        class CantFly implements Flys{

	        public String fly() {
		
		        return "I can't fly";
		
	        }
	
        }

All'interno del costruttore della classe Animal.java definisco una variabile di istanza flyingType di tipo Flys

	// Instead of using an interface in a traditional way
	// we use an instance variable that is a subclass
	// of the Flys interface.
	
	// Animal doesn't care what flyingType does, it just
	// knows the behavior is available to its subclasses
	
	// This is known as Composition : Instead of inheriting
	// an ability through inheritance the class is composed
	// with Objects with the right ability
	
	// Composition allows you to change the capabilities of 
	// objects at run time!
	
	public Flys flyingType;

	// Animal pushes off the responsibility for flying to flyingType
	
	public String tryToFly(){
		
		return flyingType.fly();
		
	}

Animal.java non sa se l'animale vola o non vola, ha solo un metodo tryToFly 
che restituisce il risultato del metodo fly che si trova all'interno dell'oggetto di tipo Flys

Ora però, poiché Animal e Flys non sanno se l'animale in questione vola devono essere le sottoclassi Dog, Cat, Bird ad impostarlo  
All'interno del costruttore In Cat e in Dog scriviamo:   

	public Cat(String name){
		
		super();
		
		setName(name);
		
		setSound("Miao");
		
		// We set the Flys interface polymorphically
		// This sets the behavior as a non-flying Animal
		
		flyingType = new CantFly();
		
	}
mentre all'interno del costruttore in Bird scriviamo:  
flyingType = new ItFlys();

Un cane e un gatto non volano e un uccello vola, ma se io volessi cambiare dinamicamente il comportamento diciamo di un cane, dicendo che vola?  
Certamente non dovrei modificare la classe Dog, perché un cane sicuramente non vola ma potrei a "run time" cambiare il comportamento di un cane particolare,
dicendo che quel cane vola.     
E allora dovrei farlo nel main e dovrei mettere a disposizione un metodo nella classe Animal per poter fare questa modifica dinamica:  

    // all'interno del costruttore della classe Animal
    // If you want to be able to change the flyingType dynamically  
	// add the following method
	
	public void setFlyingAbility(Flys newFlyType){
		
		flyingType = newFlyType;
		
	}
	
e nel main scriverei:

    Animal sparky = new Dog("Sparky");

	// This allows dynamic changes for flyingType
		
	sparky.setFlyingAbility(new ItFlys());
		
	System.out.println("(dopo il cambiamento dinamico) Dog: " + sparky.tryToFly());
	
### Observer pattern

Questa spiegazione è presa da [Youtube Derek Banas "Observer Pattern" dalla playlist Designs Patterns](https://www.youtube.com/watch?v=vNHpsC5ng_E&list=PLF206E906175C7E07)  

**Definizione**  

L'Observer pattern definisce una dipendenza uno a molti tra oggetti, cosicché quando un oggetto (the Subject) cambia stato, tutti i suoi oggetti dipendenti (the Observers) 
vengono notificati ed eventualmente aggiornati automaticamente.

**Quando usare l'Observer Pattern**

**Descrizione**

La classe concreta che implementa l'interfaccia Subject deve possedere i metodi register, unregister, notifyObserver.   
La classe concreta che implementa l'interfaccia Observer deve possedere il metodo update, che aggiorna i valori (nel caso dell'esempio di tre titoli azionari).  

Il main (in questo caso GrabSocks):  

*  crea un oggetto concreto che implementa Subject 
*  crea un oggetto concreto che implementa un Observer (observer1)
*  imposta i valori delle azioni nel Subject "concreto"
*  crea un altro Observer (observer2)
*  imposta nuovamente i valori delle azioni nel Subject "concreto" (in questo caso non importa se cambiano o no)
*  effettua unregiste di observer2
*  imposta per una terza volta i valori delle azioni nel Subject "concreto"

        public class GrabStocks{
	
        	public static void main(String[] args){
        		
        		// Create the Subject object
        		// It will handle updating all observers 
        		// as well as deleting and adding them
        		
        		StockGrabber stockGrabber = new StockGrabber();
        		
        		// Create an Observer that will be sent updates from Subject
        		
        		@SuppressWarnings("unused")
        		StockObserver observer1 = new StockObserver(stockGrabber);
        				
        		stockGrabber.setIBMPrice(197.00);
        		stockGrabber.setAAPLPrice(677.60);
        		stockGrabber.setGOOGPrice(676.40);
        		
        		StockObserver observer2 = new StockObserver(stockGrabber);
        			
        		stockGrabber.setIBMPrice(197.00);
        		stockGrabber.setAAPLPrice(677.60);
        		stockGrabber.setGOOGPrice(676.40);
        		
        		// Delete one of the observers
        		
        		stockGrabber.unregister(observer2);
        		
        		stockGrabber.setIBMPrice(197.00);
        		stockGrabber.setAAPLPrice(677.60);
        		stockGrabber.setGOOGPrice(676.40);
        		
        	}
        }

Nel subject "concreto" ad ogni set di valore di azione viene richiamato il metodo notifyObserver che cicla su tutti gli observer e per ognuno effettua update 
(sull'observer "concreto") dei valori delle azioni	 

	public void notifyObserver() {
		
		// Cycle through all observers and notifies them of
		// price changes
		
		for(Observer observer : observers){
			
			observer.update(ibmPrice, aaplPrice, googPrice);
			
		}
	}
	
	// Change prices for all stocks and notifies observers of changes
	
	public void setIBMPrice(double newIBMPrice){
		
		this.ibmPrice = newIBMPrice;
		
		notifyObserver();
		
	}
	
	public void setAAPLPrice(double newAAPLPrice){
		
		this.aaplPrice = newAAPLPrice;
		
		notifyObserver();
		
	}

	public void setGOOGPrice(double newGOOGPrice){
	
		this.googPrice = newGOOGPrice;
	
		notifyObserver();
	
	}

Nell'observer "concreto" ad ogni update vengono stampati i prezzi di tutte le azioni  

	public void update(double ibmPrice, double aaplPrice, double googPrice) {
		
		this.ibmPrice = ibmPrice;
		this.aaplPrice = aaplPrice;
		this.googPrice = googPrice;
		
		printThePrices();
		
	}
	
	public void printThePrices(){
		
		System.out.println(observerID + "\nIBM: " + ibmPrice + "\nAAPL: " + 
				aaplPrice + "\nGOOG: " + googPrice + "\n");
		
	}

Il risultato dell'elaborazione è il seguente:   

    New Observer 1
    1
    IBM: 197.0
    AAPL: 0.0
    GOOG: 0.0
    
    1
    IBM: 197.0
    AAPL: 677.6
    GOOG: 0.0
    
    1
    IBM: 197.0
    AAPL: 677.6
    GOOG: 676.4
    
    
    // da qui in poi i valori delle azioni non cambiano più   
    // ma si notificano ugualmente ad ogni "set" i valori di tutte e tre le azioni ad ogni observer registrato
    
    New Observer 2
    1
    IBM: 197.0
    AAPL: 677.6
    GOOG: 676.4
    
    2
    IBM: 197.0
    AAPL: 677.6
    GOOG: 676.4
    
    1
    IBM: 197.0
    AAPL: 677.6
    GOOG: 676.4
    
    2
    IBM: 197.0
    AAPL: 677.6
    GOOG: 676.4
    
    1
    IBM: 197.0
    AAPL: 677.6
    GOOG: 676.4
    
    2
    IBM: 197.0
    AAPL: 677.6
    GOOG: 676.4
    
    Observer 2 deleted
    1
    IBM: 197.0
    AAPL: 677.6
    GOOG: 676.4
    
    1
    IBM: 197.0
    AAPL: 677.6
    GOOG: 676.4
    
    1
    IBM: 197.0
    AAPL: 677.6
    GOOG: 676.4

### http

### https

### information systems development

### soap
from https://www.upwork.com/hiring/development/soap-vs-rest-comparing-two-apis/   
SOAP (Simple Object Access Protocol) is its own #Information-Systems-Developmentprotocol, and is a bit more mplex by defining more standards than REST—things like security and 
how messages are sent. These built-in standards do carry a bit more overhead, but can be a deciding factor for organizations that require 
more comprehensive features in the way of security, transactions, and ACID (Atomicity, Consistency, Isolation, Durability) compliance. 
For the sake of this comparison, we should point out that many of the reasons SOAP is a good choice rarely apply to web services scenarios, 
which make it more ideal for enterprise-type situations.

Reasons you may want to build an application with a SOAP API include higher levels of security 
(e.g., a mobile application interfacing with a bank), messaging apps that need reliable communication, or ACID compliance.

### computer networks
Un corso interessante che parte dalle basi è https://www.youtube.com/watch?v=PwFqQfyv0xM&list=PLrjkTql3jnm-iqlEOuVTkmCaRP8F2H3-u
*  Introduction
*  Computer networks uses - Business
*  Computer networks uses - Home
*  Transmission types - broadcast & point to point link
*  Inter processors distance of network
*  Network topologies - bus topology
*  Star topology
*  Network devices - hub, router, bridge, switches & gateway
*  LAN, WAN, MAN, PAN
*  Bluetooth
*  OSI Model
    * Physical layer
    * Data link layer
    * Network layer
    * Transport layer
    * Presentation and Session layer
    * Application layer
* Zigbee
    * Introduction
    * Working
    * Network configuration
* Protocol hierarchies in layers - network software